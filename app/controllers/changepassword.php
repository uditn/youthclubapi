<?php

class ChangePassword extends Controller
{

    /*
     * http://localhost/changepassword
     */
    function Index()
    {
        $this->changepassword();
    }

    // validating user's record
    function changepassword()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $response = [];
            $response_code = 0;

            /*
            * Every class deriving from Controller has access to
            * All helpers in /core/helpers, autoloaded
            * model() and view() methods
            */
            $this->model('user');

            // get posted data
            $data = json_decode(file_get_contents("php://input"));

            // sanitize post data
            Validation::sanitize($data);

            // Authorizing jwt token
            if($token_data = JWTOperation::validate($data->token)){

                // Checking old password is exist or not.
                if($token_data->password == sha1($data->opassword)){

                    $user = new User();

                    // set user property values
                    $user->id = $token_data->id;
                    $user->password = sha1($data->npassword);

                    // updating user's new password
                    if(
                        $data->opassword == $data->npassword ||
                        $user->updatePassword()
                    ){

                        // Adding new password hash value to token data
                        $token_data->password = $user->password;

                        // generate jwt
                        $jwt = JWTOperation::generate((array)$token_data);

                        // return user's update response
                        $response_code = 200;
                        $response = array(
                            "status" => true,
                            "response" => 200,
                            "message" => "Password Updated Successfully.",
                            "jwt" => $jwt,
                        );
                    }
                    // If updation failed.
                    else{
                        $response_code = 400;
                        $response = array(
                            "status" => false,
                            "response" => 400,
                            "message" => "Password updation failed !",
                        );
                    }
                }
                // If old password doesn't match
                else{
                    $response_code = 401;
                    $response = array(
                        "status" => false,
                        "response" => 401,
                        "message" => "Old password doesn't match !",
                    );
                }
            }
            // If authorization failed
            else{
                $response_code = 401;
                $response = array(
                    "status" => false,
                    "response" => 401,
                    "message" => "Access denied !",
                );
            }

            // set response code
            http_response_code($response_code);

            // return the response
            echo json_encode($response);

        } else {
            //  If url method is not put
            header("Location: /invalidurl");
        }
    }
}

?>