<?php

class VerifyToken extends Controller
{

    /*
     * http://localhost/verifytoken
     */
    function Index()
    {
        $this->verifytoken();
    }

    // validating user's record
    function verifytoken()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $response = [];
            $response_code = 0;

            // get posted data
            $data = json_decode(file_get_contents("php://input"));

            // sanitize post data
            Validation::sanitize($data);

            // Authorizing jwt token
            if($token_data = JWTOperation::validate($data->token)){

                    // return user's data
                    $response_code = 200;
                    $response = array(
                        "status" => true,
                        "response" => 200,
                        "message" => "Record found !",
                        "data" => (array)$token_data,
                    );
            }
            // If authorization failed
            else{
                $response_code = 401;
                $response = array(
                    "status" => false,
                    "response" => 401,
                    "message" => "Access denied !",
                );
            }

            // set response code
            http_response_code($response_code);

            // return the response
            echo json_encode($response);

        } else {
            //  If url method is not post
            header("Location: /invalidurl");
        }
    }
}

?>