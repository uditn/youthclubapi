<?php

class InvalidUrl extends Controller {

    /*
     * http://localhost/
     */
    function Index () {
        $this->invalidUrl();
    }

    function invalidUrl()
    {
        // If the url in invalid for operation

        // set response code
        http_response_code(400);

        $response = array(
            "status" => false,
            "response" => 400,
            "message" => "Invalid url"
        );

        // show error message
        echo json_encode($response);
    }
}

?>