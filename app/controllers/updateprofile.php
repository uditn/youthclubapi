<?php

class UpdateProfile extends Controller
{

    /*
     * http://localhost/updateprofile
     */
    function Index()
    {
        $this->updateProfile();
    }

    // validating user's record
    function updateProfile()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $response = [];
            $response_code = 0;

            /*
            * Every class deriving from Controller has access to
            * All helpers in /core/helpers, autoloaded
            * model() and view() methods
            */
            $this->model('user');


            // sanitize post data
            Validation::sanitize((object)$_POST);

            // Authorizing jwt token
            if($token_data = JWTOperation::validate($_POST["token"])){

                $user = new User();

                // set user property values
                Convertion::arrayToObject($user, $_POST);
                $user->id = $token_data->id;

                // Uploading image and resume.
                $user->resume_path = $this->uploadFile($_FILES["resume"], $token_data->resume_path);
                $user->avatar_path = $this->uploadFile($_FILES["avatar"], $token_data->avatar_path);

                // updating user's record
                if($user->update()){

                    // Retrieving user's record
                    $user->read();

                    // Re-generate jwt
                    $jwt = JWTOperation::generate(Convertion::objectToArray($user));

                    // return user's update response
                    $response_code = 200;
                    $response = array(
                        "status" => true,
                        "response" => 200,
                        "message" => "Profile updated successfully !",
                        "jwt" => $jwt,
                    );
                }
                // If updation failed.
                else{
                    $response_code = 400;
                    $response = array(
                        "status" => false,
                        "response" => 400,
                        "message" => "profile updation failed !",
                    );
                }
            }
            // If authorization failed
            else{
                $response_code = 401;
                $response = array(
                    "status" => false,
                    "response" => 401,
                    "message" => "Access denied !",
                );
            }

            // set response code
            http_response_code($response_code);

            // return the response
            echo json_encode($response);

        } else {
            //  If url method is not post
            header("Location: /invalidurl");
        }
    }


    // Upload file to server
    function uploadFile($file,$path){

        // Checking requested file is valid or not
        if(!empty($file["name"])){

            // Extracting user's location from previous path
            $directories= explode("/",$path);

            // moving to users global directory
            chdir(ROOT."/app/users");

            // Checking -user's directory exists or not
            if(!file_exists($directories[0])){

                // Making target location for user's
                $target_dir = substr($_POST["email"],0,strpos($_POST["email"],"@"))."_".date('Y-m-d_H:i:s');

                // If user's directory is not exist then create new directory
                mkdir($target_dir,0777,true);

                // Moving to user's direcotroy
                $path = $target_dir;
                chdir(ROOT."/app/users/".$path);

            }else{
                chdir(ROOT."/app/users/".$directories[0]);
                $path = $directories[0];
            }

            // retrieving extension from file
            $extension = pathinfo($file["name"],PATHINFO_EXTENSION);
            // Making target location for file
            $target_file = basename($file["name"], ".$extension")."_".date('Y-m-d_H:i:s').".".$extension; 

            // Deleting old file if exist.
            unlink($directories[1]);
            // Moving new file to user's directory
            move_uploaded_file($file["tmp_name"], $target_file);

            return $path."/".$target_file;
        }else{
            return $path;
        }
    }
}

?>