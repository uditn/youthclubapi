<?php

class Register extends Controller
{

    /*
     * http://localhost/register
     */
    function Index()
    {
        $this->register();
    }

    // Register user's record
    function register()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $response = [];
            $response_code = 0;
            /*
            * Every class deriving from Controller has access to
            * All helpers in /core/helpers, autoloaded
            * model() and view() methods
            */
            $this->model('user');

            // get posted data
            $data = json_decode(file_get_contents("php://input"));

            // sanitize post data
            Validation::sanitize($data);

            $user = new User();

            // set user property values
            $data->password = sha1($data->password);
            Convertion::arrayToObject($user, (array)$data);

            // cheching is user already exist
            if (
                !empty($user->email) &&
                !$user->userExists()
            ) {
                // register the user
                if (
                    !empty($user->first_name) &&
                    !empty($user->last_name) &&
                    !empty($user->mobile_number) &&
                    !empty($user->email) &&
                    !empty($user->password) &&
                    $user->create()
                ) {

                    // If user created successfully
                    $response_code = 201;
                    $response = array(
                        "status" => true,
                        "response" => 201,
                        "message" => "User successfully registered.",
                    );
                }

                // If unable to create user
                else {

                    $response_code = 400;
                    $response = array(
                        "status" => false,
                        "response" => 400,
                        "message" => "User registration failed !",
                    );
                }

                // response if user already exists.
            } else {

                $response_code = 400;
                $response = array(
                    "status" => false,
                    "response" => 400,
                    "message" => "User already exists !",
                );
            }

            // set response code
            http_response_code($response_code);

            // return the response
            echo json_encode($response);
        } else {
            //  If url method is not post
            header("Location: /invalidurl");
        }
    }
}

?>