<?php

class Login extends Controller
{

    /*
     * http://localhost/login
     */
    function Index()
    {
        $this->login();
    }

    // validating user's record
    function login()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $response = [];
            $response_code = 0;

            /*
            * Every class deriving from Controller has access to
            * All helpers in /core/helpers, autoloaded
            * model() and view() methods
            */
            $this->model('user');

            // get posted data
            $data = json_decode(file_get_contents("php://input"));

            // sanitize post data
            Validation::sanitize($data);

            $user = new User();

            // set user property values
            $user->email = $data->email;

            // Authorizing user credential
            if (
                $user->userExists() &&
                sha1($data->password) == $user->password

            ) {

                // Retrieving user's data
                $user->read();

                // generate jwt
                $jwt = JWTOperation::generate(Convertion::objectToArray($user));

                // response if authorization succeed
                $response_code = 200;
                $response = array(
                    "status" => true,
                    "response" => 200,
                    "message" => "Successful login !",
                    "jwt" => $jwt,
                );
            }
            // response if authorization failed
            else {

                $response_code = 401;
                $response = array(
                    "status" => false,
                    "response" => 401,
                    "message" => "User credential mismatched",
                );
            }

            // set response code
            http_response_code($response_code);

            // return the response
            echo json_encode($response);
        } else {
            //  If url method is not post
            header("Location: /invalidurl");
        }
    }
}
