<?php

/*
 * Every class derriving from Model has access to $this->db
 * $this->db is a PDO object
 * Has a config in /core/config/database.php
 */
class User extends Model
{

    public $id = "";
    public $first_name = "";
    public $last_name = "";
    public $email = "";
    public $password = "";
    public $mobile_number = "";
    public $gender = "";
    public $state = "";
    public $skills = [];
    public $age = "";
    public $resume_path = "";
    public $avatar_path = "";

    // check if given email exist in the database
    function userExists()
    {
        $status = false;

        try{
            // query to check if email exists
            $query = "SELECT id, password FROM  users
            WHERE email = ? LIMIT 0,1";

            // prepare the query
            $stmt = $this->db->prepare($query);

            // bind given email value
            $stmt->bindParam(1, $this->email);

            // execute the query
            $stmt->execute();

            // get number of rows
            $num = $stmt->rowCount();

            // if email exists, assign values to object properties
            if ($num > 0) {

                // get record details / values
                $row = $stmt->fetch(PDO::FETCH_ASSOC);

                // assign values to object properties
                $this->id = $row["id"];
                $this->password = $row["password"];


                // return true because email exists in the database
                $status = true;
            }
        } catch (Exception $e) {
            //echo 'Caught exception: ',  $e->getMessage();
        }

        // return false if email does not exist in the database
        return $status;
    }


    // Update user's new password
    function updatePassword()
    {
        $status = false;

        try{
            // query to check if email exists
            $query = "UPDATE users SET password = :password WHERE id = :id";

            // prepare the query
            $stmt = $this->db->prepare($query);

            // bind given id and new password value
            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':password', $this->password);

            // execute the query
            $stmt->execute();

            ;

            // return true if updation succeed
            if ($stmt->rowCount() > 0) {
                $status = true;
            }
        } catch (Exception $e) {
            //echo 'Caught exception: ',  $e->getMessage();
        }

        // return false if updation failed
        return $status;
    }

    // Fetch records from users table
    function read()
    {
        $status = false;

        try{
            // Query for users data
            $query = "SELECT * FROM users WHERE id= ?";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();

            // get number of rows
            $num = $stmt->rowCount();

            if ($num > 0) {

                // get record details / values
                $row = $stmt->fetch(PDO::FETCH_ASSOC);

                // assign values to object properties
                Convertion::arrayToObject($this,$row);

                // Fetch records from skills table
                $query_skills = "SELECT GROUP_CONCAT(skills.name) as skill_names
                                 FROM skills JOIN user_skill 
                                 ON skills.id = user_skill.fk_skill_id 
                                 WHERE user_skill.fk_user_id = ?";

                $stmt_skills = $this->db->prepare($query_skills);
                $stmt_skills->execute([$this->id]);
                $skills_row =  $stmt_skills->fetch(PDO::FETCH_ASSOC);

                // assign skills to object properties
                $this->skills = explode(",", $skills_row['skill_names']);

                // return true all records are fetched
                $status = true;
            }
        } catch (Exception $e) {
            //echo 'Caught exception: ',  $e->getMessage();
        }

        // return false if retrieving failed
        return $status;
    }

    // Create record on database.
    public function create()
    {
        $status = false;

        try {

            $query = "INSERT INTO users(first_name, last_name, email, password, mobile_number) 
                        VALUES (:firstname, :lastname, :email, :password, :mobile_number)";

            $stmt = $this->db->prepare($query);

            $stmt->bindParam(':firstname', $this->first_name);
            $stmt->bindParam(':lastname', $this->last_name);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':mobile_number', $this->mobile_number);
            $stmt->bindParam(':password', $this->password);

            $stmt->execute();

            if ($stmt->rowCount() > 0) {
                $status = true;
            }
        } catch (Exception $e) {
            //echo 'Caught exception: ',  $e->getMessage();
        }

        return $status;
    }

    // Fetch records from users table
    function update()
    {
        $status = false;

        try{

            // Starting the transaction
            $this->db->beginTransaction();

            $query = "UPDATE users SET first_name=:first_name,last_name=:last_name,
                      mobile_number=:mobile_number,gender=:gender,state=:state,age=:age,
                      resume_path=:resume_path,avatar_path=:avatar_path WHERE id=:id";

            $stmt = $this->db->prepare($query);

            $stmt->bindParam(':first_name', $this->first_name);
            $stmt->bindParam(':last_name', $this->last_name);
            $stmt->bindParam(':mobile_number',$this->mobile_number);
            $stmt->bindParam(':gender', $this->gender);
            $stmt->bindParam(':state', $this->state);
            $stmt->bindParam(':age', $this->age);
            $stmt->bindParam(':resume_path', $this->resume_path);
            $stmt->bindParam(':avatar_path', $this->avatar_path);
            $stmt->bindParam(':id', $this->id);

            $stmt->execute();

            if(count($this->skills) != 0){

                //Storing skills
                $temp_skills = array("PHP"=>1,"Java"=>2,"HTML"=>3,"CSS"=>4,"Bootstrap"=>5,"MYSQL"=>6);

                //Deleting the previous skills
                $delete = $this->db->prepare("DELETE FROM user_skill WHERE fk_user_id = ?");
                $delete->execute([$this->id]);

                //Adding updated skills
                foreach($this->skills as $skill){

                    $query_skills = "INSERT INTO user_skill(fk_user_id,fk_skill_id) VALUES(:user_id,:skill_id)";
                    $stmt_insert_skills = $this->db->prepare($query_skills);
                    $stmt_insert_skills->bindParam(':user_id',$this->id);
                    $stmt_insert_skills->bindParam(':skill_id',$temp_skills[$skill]);
                    $stmt_insert_skills->execute();

                }
            }

            // commit the transaction
            $this->db->commit();

            // return true record updated successfully
            $status = true;

        } catch (Exception $e) {
            //echo 'Caught exception: ',  $e->getMessage();
        }

        // return false if retrieving failed
        return $status;
    }
}

