<?php

/*
 * Convertion Class
 */
class Convertion {

    // Converting array to object
    public static function arrayToObject (User $user,$array) {

        // assign values to object properties
        $user->id = $array["id"];
        $user->first_name = $array["first_name"];
        $user->last_name = $array["last_name"];
        $user->email = $array["email"];
        $user->password = $array["password"];
        $user->mobile_number = $array["mobile_number"];
        $user->gender = $array["gender"];
        $user->state = $array["state"];
        $user->skills = $array["skills"];
        $user->age = $array["age"];
        $user->avatar_path = $array["avatar_path"];
        $user->resume_path = $array["resume_path"];

    }

    // Converting object to array
    public static function objectToArray ($data) {

        $array = array();

        // assign object properties to array
        $array["id"] = $data->id; 
        $array["first_name"] = $data->first_name;
        $array["last_name"] = $data->last_name;
        $array["email"] = $data->email;
        $array["password"] = $data->password;
        $array["mobile_number"] = $data->mobile_number;
        $array["gender"] = $data->gender;
        $array["state"] = $data->state;
        $array["skills"] = $data->skills;
        $array["age"] = $data->age;
        $array["avatar_path"] = $data->avatar_path;
        $array["resume_path"] = $data->resume_path;

        return $array; 

    }
}

?>