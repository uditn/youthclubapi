<?php

/*
 * VALIDATOIN CLASS
 */
class Validation {

    // Sanitizing request data
    public static function sanitize ($data) {

        foreach ((array)$data as $key => $value) {

            if($key != "skills"){
                $data->$key = htmlspecialchars(strip_tags($value));
            }
        }

    }

}

?>