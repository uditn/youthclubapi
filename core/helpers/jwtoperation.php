
<?php

/*
 * JwtOperation CLASS
 */
require("core/lib/php-jwt/src/BeforeValidException.php");
require("core/lib/php-jwt/src/ExpiredException.php");
require("core/lib/php-jwt/src/SignatureInvalidException.php");
require("core/lib/php-jwt/src/JWT.php");
require("core/config/jwtconfig.php");

use \Firebase\JWT\JWT;

 // show error reporting
 error_reporting(E_ALL);

 // set your default time-zone
 date_default_timezone_set('Asia/Kolkata');

class JWTOperation extends JWTConfig
{

    // Generating jwt token
    public static function  generate($data)
    {
        $token = array(
            "iss" => self::$iss,
            "aud" => self::$aud,
            "iat" => self::$iat,
            "nbf" => self::$nbf,
            "data" => $data,
        );

        return JWT::encode($token, self::$key);
    }

    // Validation jwt token
    public static function validate($token)
    {
        try {

            // decode jwt
            $decoded = JWT::decode($token, self::$key, array('HS256'));
            return $decoded->data;

        }catch (Exception $e){

            //echo 'Caught exception: ',  $e->getMessage();
        }

        return false;
    }
}

?>