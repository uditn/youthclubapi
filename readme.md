
Database  : youthclub
======================
CREATE DATABASE IF NOT EXIST youthclub;

Table : users
==================

CREATE TABLE users
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(255),
	last_name VARCHAR(255),
	email VARCHAR(255) UNIQUE NOT NULL,
	password VARCHAR(255) NOT NULL,
	mobile_number BIGINT(10) UNSIGNED NULL,
	gender VARCHAR(10) DEFAULT "Male",
	state VARCHAR(50),
	age INT(2) UNSIGNED NULL,
	resume_path	 VARCHAR(255),
	avatar_path VARCHAR(255),
	created_date_time TIMESTAMP DEFAULT NOW(),
	last_updated_date_time TIMESTAMP ON UPDATE NOW() DEFAULT NOW(),
	PRIMARY KEY(id)
);


// create an index
CREATE INDEX email ON users(email);


Table: skills
===============

CREATE TABLE skills
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

// Insert skills  in skills tables

INSERT INTO skills VALUES(1, "PHP"), (2, "Java"),
(3, "HTML"), (4, "CSS"), (5, "Bootstrap"), (6, "MYSQL");

Table : user_skill
=======================

CREATE TABLE user_skill
(
	fk_user_id INT UNSIGNED NOT NULL,
	fk_skill_id INT UNSIGNED NOT NULL,
	FOREIGN KEY (fk_user_id) REFERENCES users(id),
	FOREIGN KEY (fk_skill_id) REFERENCES skills(id)
);
